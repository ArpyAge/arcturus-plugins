package eu.furnibuilder.furnidata;

import com.eu.habbo.Emulator;
import com.eu.habbo.habbohotel.commands.Command;
import com.eu.habbo.habbohotel.gameclients.GameClient;
import com.eu.habbo.plugin.EventHandler;
import com.eu.habbo.plugin.EventListener;
import com.eu.habbo.plugin.events.users.UserExitRoomEvent;

public class FurniDataCommand extends Command implements EventListener
{
    public static String FURNIDATA_KEY = "furnidata.ison";
    public FurniDataCommand(String permission, String[] keys)
    {
        super(permission, keys);

        Emulator.getPluginManager().registerEvents(FurniData.INSTANCE, this);
    }

    @Override
    public boolean handle(GameClient gameClient, String[] strings) throws Exception
    {
        if(gameClient.getHabbo().getHabboStats().cache.containsKey(FURNIDATA_KEY)) {
            gameClient.getHabbo().getHabboStats().cache.remove(FURNIDATA_KEY);
            gameClient.getHabbo().whisper(Emulator.getTexts().getValue("furnidata.cmd_furnidata.off"));
        }
        else {
            gameClient.getHabbo().getHabboStats().cache.put(FURNIDATA_KEY, true);
            gameClient.getHabbo().whisper(Emulator.getTexts().getValue("furnidata.cmd_furnidata.on"));
        }
        return true;
    }

    @EventHandler
    public static void onUserExitRoomEvent(UserExitRoomEvent event)
    {
        event.habbo.getHabboStats().cache.remove(FURNIDATA_KEY);
    }

}
