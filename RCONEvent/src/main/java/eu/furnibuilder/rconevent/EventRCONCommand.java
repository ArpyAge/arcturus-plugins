package eu.furnibuilder.rconevent;

import com.eu.habbo.Emulator;
import com.eu.habbo.habbohotel.rooms.Room;
import com.eu.habbo.habbohotel.users.Habbo;
import com.eu.habbo.messages.ServerMessage;
import com.eu.habbo.messages.outgoing.generic.alerts.BubbleAlertComposer;
import com.eu.habbo.messages.rcon.RCONMessage;
import com.google.gson.Gson;
import gnu.trove.map.hash.THashMap;

import java.util.Map;

public class EventRCONCommand extends RCONMessage<EventRCONCommand.JSONRCONEvent>
{
    public EventRCONCommand()
    {
        super(EventRCONCommand.JSONRCONEvent.class);
    }

    public void handle(Gson gson, JSONRCONEvent object)
    {
        Habbo habbo = Emulator.getGameEnvironment().getHabboManager().getHabbo(object.user_id);
        if(habbo != null) {

            int room_id = 0;
            String room_name = "";

            if(object.room_id != 0) {
                room_id = object.room_id;
                Room room = Emulator.getGameEnvironment().getRoomManager().getRoom(room_id);
                room_name = room.getName();
            }
            else if(habbo.getHabboInfo().getCurrentRoom() != null) {
                room_id = habbo.getHabboInfo().getCurrentRoom().getId();
                room_name = habbo.getHabboInfo().getCurrentRoom().getName();
            }

            if(room_id > 0 && !room_name.equals("")) {
                THashMap<String, String> codes = new THashMap<>();
                codes.put("ROOMNAME", room_name);
                codes.put("ROOMID", room_id + "");
                codes.put("USERNAME", habbo.getHabboInfo().getUsername());
                codes.put("LOOK", habbo.getHabboInfo().getLook());
                codes.put("TIME", Emulator.getDate().toString());
                codes.put("MESSAGE", object.message);

                ServerMessage msg = new BubbleAlertComposer("hotel.event", codes).compose();

                for(Map.Entry<Integer, Habbo> set : Emulator.getGameEnvironment().getHabboManager().getOnlineHabbos().entrySet())
                {
                    Habbo alertHabbo = set.getValue();
                    if(alertHabbo.getHabboStats().blockStaffAlerts)
                        continue;

                    alertHabbo.getClient().sendResponse(msg);
                }
            }
        }
    }

    static class JSONRCONEvent
    {
        public int user_id;
        public String message;
        public int room_id = 0;
    }
}
