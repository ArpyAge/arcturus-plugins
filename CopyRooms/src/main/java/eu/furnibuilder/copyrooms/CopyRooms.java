package eu.furnibuilder.copyrooms;

import com.eu.habbo.Emulator;
import com.eu.habbo.habbohotel.commands.CommandHandler;
import com.eu.habbo.habbohotel.users.Habbo;
import com.eu.habbo.plugin.EventHandler;
import com.eu.habbo.plugin.EventListener;
import com.eu.habbo.plugin.HabboPlugin;
import com.eu.habbo.plugin.events.emulator.EmulatorLoadedEvent;
import eu.furnibuilder.copyrooms.commands.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class CopyRooms extends HabboPlugin implements EventListener
{
    public static CopyRooms INSTANCE = null;

    @Override
    public void onEnable()
    {
        INSTANCE = this;
        Emulator.getPluginManager().registerEvents(this, this);

        if (Emulator.isReady)
        {
            this.checkDatabase();
        }

        Emulator.getLogging().logStart("[CopyRooms] Started CopyRooms Commands Plugin!");
    }

    @Override
    public void onDisable()
    {
        Emulator.getLogging().logShutdownLine("[CopyRooms] Stopped CopyRooms Commands Plugin!");
    }

    @EventHandler
    public static void onEmulatorLoaded(EmulatorLoadedEvent event)
    {
        INSTANCE.checkDatabase();
    }

    @Override
    public boolean hasPermission(Habbo habbo, String s)
    {
        return false;
    }

    private void checkDatabase()
    {
        boolean reloadPermissions = false;

        try (Connection connection = Emulator.getDatabase().getDataSource().getConnection(); Statement statement = connection.createStatement())
        {
            statement.execute("ALTER TABLE  `emulator_texts` CHANGE  `value`  `value` VARCHAR( 4096 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL");
        }
        catch (SQLException e){}
        Emulator.getTexts().register("copyrooms.cmd_copyroom.keys", "copyroom;cr");
        Emulator.getTexts().register("commands.description.cmd_copyroom", ":copyroom");
        reloadPermissions = this.registerPermission("cmd_copyroom", "'0', '1'", "0", reloadPermissions);

        Emulator.getTexts().register("copyrooms.cmd_takeroom.keys", "takeroom;tr");
        Emulator.getTexts().register("commands.description.cmd_takeroom", ":takeroom");
        Emulator.getTexts().register("copyrooms.takeroom.has_guild", "This room has a guild and therefor cannot be taken.");
        Emulator.getTexts().register("copyrooms.takeroom.is_owner", "This room is already yours.");
        reloadPermissions = this.registerPermission("cmd_takeroom", "'0', '1'", "0", reloadPermissions);

        if (reloadPermissions)
        {
            Emulator.getGameEnvironment().getPermissionsManager().reload();
        }

        CommandHandler.addCommand(new CopyRoomCommand("cmd_copyroom", Emulator.getTexts().getValue("copyrooms.cmd_copyroom.keys").split(";")));
        CommandHandler.addCommand(new TakeRoomCommand("cmd_takeroom", Emulator.getTexts().getValue("copyrooms.cmd_takeroom.keys").split(";")));
    }

    private boolean registerPermission(String name, String options, String defaultValue, boolean defaultReturn)
    {
        try (Connection connection = Emulator.getDatabase().getDataSource().getConnection())
        {
            try (PreparedStatement statement = connection.prepareStatement("ALTER TABLE  `permissions` ADD  `" + name +"` ENUM(  " + options + " ) NOT NULL DEFAULT  '" + defaultValue + "'"))
            {
                statement.execute();
                return true;
            }
        }
        catch (SQLException e)
        {}

        return defaultReturn;
    }

    public static void main(String[] args)
    {
        System.out.println("Don't run this seperately");
    }
}