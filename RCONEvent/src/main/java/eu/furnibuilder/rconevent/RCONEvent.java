package eu.furnibuilder.rconevent;

import com.eu.habbo.Emulator;
import com.eu.habbo.habbohotel.users.Habbo;
import com.eu.habbo.plugin.EventHandler;
import com.eu.habbo.plugin.EventListener;
import com.eu.habbo.plugin.HabboPlugin;
import com.eu.habbo.plugin.events.emulator.EmulatorLoadedEvent;

public class RCONEvent extends HabboPlugin implements EventListener {

    @Override
    public void onEnable()
    {
        Emulator.getPluginManager().registerEvents(this, this);

        if (Emulator.isReady)
        {
            Emulator.getRconServer().addRCONMessage("event", EventRCONCommand.class);
        }

        Emulator.getLogging().logStart("[RCONEvent] Started RCONEvent Plugin!");
    }

    @Override
    public void onDisable()
    {
        Emulator.getLogging().logShutdownLine("[RCONEvent] Stopped RCONEvent Plugin!");
    }

    @Override
    public boolean hasPermission(Habbo habbo, String s)
    {
        return false;
    }

    @EventHandler
    public static void systemLoaded(EmulatorLoadedEvent event) throws Exception
    {
        Emulator.getRconServer().addRCONMessage("event", EventRCONCommand.class);
    }

    public static void main(String[] args)
    {
        System.out.println("Don't run this seperately");
    }
}
