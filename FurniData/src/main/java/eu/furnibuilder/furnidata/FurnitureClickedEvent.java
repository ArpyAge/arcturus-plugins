package eu.furnibuilder.furnidata;

import com.eu.habbo.habbohotel.rooms.Room;
import com.eu.habbo.habbohotel.users.HabboItem;
import com.eu.habbo.messages.ICallable;
import com.eu.habbo.messages.incoming.MessageHandler;

public class FurnitureClickedEvent implements ICallable {
    @Override
    public void call(MessageHandler messageHandler) {
        if(messageHandler.client.getHabbo().getHabboStats().cache.containsKey(FurniDataCommand.FURNIDATA_KEY)) {
            messageHandler.isCancelled = true;
            Room room = messageHandler.client.getHabbo().getHabboInfo().getCurrentRoom();
            if (room != null) {

                int itemId = messageHandler.packet.readInt();
                int state = messageHandler.packet.readInt();

                HabboItem item = room.getHabboItem(itemId);

                String message = "Information for item: <b>" + item.getBaseItem().getFullName() + "</b>\r\n" +
                        "<b>items_base table</b>\r\n" +
                        "- id: " + item.getBaseItem().getId() + "\r" +
                        "- sprite_id: " + item.getBaseItem().getSpriteId() + "\r" +
                        "- Width:  " + item.getBaseItem().getWidth() + "\r" +
                        "- Length:  " + item.getBaseItem().getWidth() + "\r" +
                        "- Stack height:  " + String.format("%f", item.getBaseItem().getHeight()).replace(',','.') + "\r" +
                        "- Allow stack:  " + item.getBaseItem().allowStack() + "\r" +
                        "- Allow walk:  " + item.getBaseItem().allowWalk() + "\r" +
                        "- Allow sit:  " + item.getBaseItem().allowSit() + "\r" +
                        "- Allow lay:  " + item.getBaseItem().allowLay() + "\r" +
                        "- Allow recycle:  " + item.getBaseItem().allowRecyle() + "\r" +
                        "- Allow trade:  " + item.getBaseItem().allowTrade() + "\r" +
                        "- Allow marketplace sell:  " + item.getBaseItem().allowMarketplace() + "\r" +
                        "- Allow gift:  " + item.getBaseItem().allowGift() + "\r" +
                        "- Allow inventory stack:  " + item.getBaseItem().allowInventoryStack() + "\r" +
                        "- Interaction type:  " + item.getBaseItem().getInteractionType().getName() + "\r" +
                        "- Interaction count:  " + item.getBaseItem().getStateCount() + "\r" +
                        "- Vending ids:  ";
                for (int i = 0; i < item.getBaseItem().getVendingItems().size(); i++) {
                    message += item.getBaseItem().getVendingItems().get(i) + ", ";
                }
                message = message.substring(0, message.length() - 2);
                message += "\r" +
                        "- effect id male:  " + item.getBaseItem().getEffectM() + "\r" +
                        "- effect id female:  " + item.getBaseItem().getEffectF() + "\r" +
                        "- Multi height:  ";
                for (int i = 0; i < item.getBaseItem().getMultiHeights().length; i++) {
                    message += item.getBaseItem().getMultiHeights()[i] + ", ";
                }
                message = message.substring(0, message.length() - 2);
                message += "\r\n" +
                        "<b>items/room</b>\r\n" +
                        "- item id: " + item.getId() + "\r" +
                        "- user id: " + item.getUserId() + "\r" +
                        "- x:  " + item.getX() + "\r" +
                        "- y:  " + item.getY() + "\r" +
                        "- z:  " + item.getZ() + "\r" +
                        "- State:  " + state + "\r";

                messageHandler.client.getHabbo().alert(message);
            }
        }
    }
}
