package eu.furnibuilder.tptome;

import com.eu.habbo.Emulator;
import com.eu.habbo.habbohotel.commands.CommandHandler;
import com.eu.habbo.habbohotel.users.Habbo;
import com.eu.habbo.plugin.EventHandler;
import com.eu.habbo.plugin.EventListener;
import com.eu.habbo.plugin.HabboPlugin;
import com.eu.habbo.plugin.events.emulator.EmulatorLoadedEvent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class TpToMe extends HabboPlugin implements EventListener {
    public static TpToMe INSTANCE = null;

    @Override
    public void onEnable() {
        INSTANCE = this;
        Emulator.getPluginManager().registerEvents(this, this);

        if (Emulator.isReady) {
            this.checkDatabase();
        }

        Emulator.getLogging().logStart("[TpToMe] Started Teleport to me Command Plugin!");
    }

    @Override
    public void onDisable() {
        Emulator.getLogging().logShutdownLine("[TpToMe] Stopped Teleport to me Command Plugin!");
    }

    @EventHandler
    public static void onEmulatorLoaded(EmulatorLoadedEvent event) {
        INSTANCE.checkDatabase();
    }

    @Override
    public boolean hasPermission(Habbo habbo, String s) {
        return false;
    }

    private void checkDatabase() {
        boolean reloadPermissions = false;

        try (Connection connection = Emulator.getDatabase().getDataSource().getConnection(); Statement statement = connection.createStatement()) {
            statement.execute("ALTER TABLE  `emulator_texts` CHANGE  `value`  `value` VARCHAR( 4096 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL");
        } catch (SQLException e) {
        }
        Emulator.getTexts().register("tptome.cmd_tptome.keys", "tptome;tp");
        Emulator.getTexts().register("commands.description.cmd_tptome", ":tptome <username>");
        Emulator.getTexts().register("tptome.error.not_found", "Could not find %user%");
        Emulator.getTexts().register("tptome.error.tp_self", "You cannot teleport yourself in front of you");
        Emulator.getTexts().register("commands.success.cmd_tptome.tptome", "* Teleports %user% to %gender_name% *");
        reloadPermissions = this.registerPermission("cmd_tptome", "'0', '1', '2'", "0", reloadPermissions);

        if (reloadPermissions) {
            Emulator.getGameEnvironment().getPermissionsManager().reload();
        }

        CommandHandler.addCommand(new TpToMeCommand("cmd_tptome", Emulator.getTexts().getValue("tptome.cmd_tptome.keys").split(";")));
    }

    private boolean registerPermission(String name, String options, String defaultValue, boolean defaultReturn) {
        try (Connection connection = Emulator.getDatabase().getDataSource().getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement("ALTER TABLE  `permissions` ADD  `" + name + "` ENUM(  " + options + " ) NOT NULL DEFAULT  '" + defaultValue + "'")) {
                statement.execute();
                return true;
            }
        } catch (SQLException e) {
        }

        return defaultReturn;
    }

    public static void main(String[] args) {
        System.out.println("Don't run this seperately");
    }
}