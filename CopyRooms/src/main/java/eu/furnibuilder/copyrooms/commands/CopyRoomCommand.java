package eu.furnibuilder.copyrooms.commands;

import com.eu.habbo.habbohotel.commands.Command;
import com.eu.habbo.habbohotel.gameclients.GameClient;
import com.eu.habbo.habbohotel.rooms.Room;
import com.eu.habbo.habbohotel.rooms.RoomChatMessageBubbles;
import com.eu.habbo.messages.outgoing.rooms.ForwardToRoomComposer;
import eu.furnibuilder.copyrooms.models.RoomObject;

public class CopyRoomCommand extends Command
{
    public CopyRoomCommand(String permission, String[] keys)
    {
        super(permission, keys);
    }

    @Override
    public boolean handle(GameClient gameClient, String[] strings) throws Exception
    {
        Room room = gameClient.getHabbo().getHabboInfo().getCurrentRoom();
        if (room != null)
        {
            gameClient.getHabbo().whisper("Copy Room: " + room.getName(), RoomChatMessageBubbles.ALERT);

            RoomObject roomObject = new RoomObject(room);

            gameClient.getHabbo().whisper("Copy Room: Applying paint and wallpaper...", RoomChatMessageBubbles.ALERT);
            int newRoomId = roomObject.insertRoom(gameClient.getHabbo());

            gameClient.getHabbo().whisper("Copy Room: Decorating Room...", RoomChatMessageBubbles.ALERT);
            roomObject.insertFurniture();

            if (newRoomId > 0)
            {
                gameClient.getHabbo().whisper("Copy Room: Finished!");
                gameClient.sendResponse(new ForwardToRoomComposer(newRoomId));
            }
        }

        return true;
    }
}