package eu.furnibuilder.copyrooms.commands;

import com.eu.habbo.Emulator;
import com.eu.habbo.habbohotel.commands.Command;
import com.eu.habbo.habbohotel.gameclients.GameClient;
import com.eu.habbo.habbohotel.rooms.Room;
import com.eu.habbo.habbohotel.rooms.RoomChatMessage;
import com.eu.habbo.habbohotel.rooms.RoomChatMessageBubbles;
import com.eu.habbo.habbohotel.users.Habbo;
import com.eu.habbo.habbohotel.users.HabboItem;
import com.eu.habbo.messages.ServerMessage;
import com.eu.habbo.messages.outgoing.rooms.ForwardToRoomComposer;
import com.eu.habbo.messages.outgoing.rooms.users.RoomUserWhisperComposer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class TakeRoomCommand extends Command
{
    public TakeRoomCommand(String permission, String[] keys)
    {
        super(permission, keys);
    }

    @Override
    public boolean handle(GameClient gameClient, String[] strings) throws Exception
    {
        final Room room = gameClient.getHabbo().getHabboInfo().getCurrentRoom();

        if (room.hasGuild())
        {
            gameClient.sendResponse(new RoomUserWhisperComposer(new RoomChatMessage(Emulator.getTexts().getValue("copyrooms.takeroom.has_guild"), gameClient.getHabbo(), gameClient.getHabbo(), RoomChatMessageBubbles.ALERT)));
            return true;
        }

        if (room.getOwnerId() == gameClient.getHabbo().getHabboInfo().getId())
        {
            gameClient.sendResponse(new RoomUserWhisperComposer(new RoomChatMessage(Emulator.getTexts().getValue("copyrooms.takeroom.is_owner"), gameClient.getHabbo(), gameClient.getHabbo(), RoomChatMessageBubbles.ALERT)));
            return true;
        }

        synchronized (room) {
            for (HabboItem item : room.getFloorItems()) {
                item.setUserId(gameClient.getHabbo().getHabboInfo().getId());
                item.needsUpdate(true);
            }

            for (HabboItem item : room.getWallItems()) {
                    item.setUserId(gameClient.getHabbo().getHabboInfo().getId());
                    item.needsUpdate(true);
            }

            room.setOwnerId(gameClient.getHabbo().getHabboInfo().getId());
            room.setOwnerName(gameClient.getHabbo().getHabboInfo().getUsername());
            room.setNeedsUpdate(true);
        }

        Collection<Habbo> habbos = new ArrayList(room.getHabbos());
        Emulator.getGameEnvironment().getRoomManager().unloadRoom(room);
        ServerMessage message = (new ForwardToRoomComposer(room.getId())).compose();
        Iterator var4 = habbos.iterator();

        while(var4.hasNext()) {
            Habbo habbo = (Habbo)var4.next();
            habbo.getClient().sendResponse(message);
        }

        return true;
    }
}