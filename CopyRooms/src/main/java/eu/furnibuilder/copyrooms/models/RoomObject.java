package eu.furnibuilder.copyrooms.models;

import com.eu.habbo.Emulator;
import com.eu.habbo.habbohotel.items.Item;
import com.eu.habbo.habbohotel.items.interactions.wired.conditions.*;
import com.eu.habbo.habbohotel.items.interactions.wired.effects.*;
import com.eu.habbo.habbohotel.items.interactions.wired.triggers.WiredTriggerBotReachedFurni;
import com.eu.habbo.habbohotel.items.interactions.wired.triggers.WiredTriggerFurniStateToggled;
import com.eu.habbo.habbohotel.items.interactions.wired.triggers.WiredTriggerHabboWalkOffFurni;
import com.eu.habbo.habbohotel.items.interactions.wired.triggers.WiredTriggerHabboWalkOnFurni;
import com.eu.habbo.habbohotel.rooms.Room;
import com.eu.habbo.habbohotel.users.Habbo;
import com.eu.habbo.habbohotel.users.HabboItem;
import gnu.trove.map.hash.THashMap;
import gnu.trove.set.hash.THashSet;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RoomObject
{
    public final String name;
    public final String description;
    public final String model;
    public final String password;
    public final int usersMax;
    public final String paperFloor;
    public final String paperWall;
    public final String paperLandscape;
    public final int thicknessWall;
    public final int wallHeight;
    public final int thicknessFloor;
    public final String moodlightData;
    public final int rollerSpeed;
    public final RoomModelObject modelObject;
    public final FurniObject[] items;

    private transient int newRoomId;
    private transient THashMap<Integer, Integer> originalToNewFurniIdMap;
    private transient THashMap<Integer, Integer> newToOriginalFurniIdMap;
    public transient THashMap<String, Integer> missingFurnitureMap = new THashMap<>();
    private transient int ownerId;
    private transient String ownerName;

    public RoomObject(Room room)
    {
        this.name = room.getName();
        this.description = room.getDescription();
        this.model = room.getLayout().getName();
        this.password = room.getPassword();
        this.usersMax = room.getUsersMax();
        this.paperFloor = room.getFloorPaint();
        this.paperWall = room.getWallPaint();
        this.paperLandscape = room.getBackgroundPaint();
        this.thicknessWall = room.getWallSize();
        this.wallHeight = room.getWallHeight();
        this.thicknessFloor = room.getFloorSize();
        this.moodlightData = "";
        this.rollerSpeed = room.getRollerSpeed();

        if (room.hasCustomLayout())
        {
            this.modelObject = new RoomModelObject(room.getLayout().getDoorX(), room.getLayout().getDoorY(), room.getLayout().getDoorDirection(), room.getLayout().getHeightmap());
        }
        else
        {
            this.modelObject = null;
        }

        THashSet<HabboItem> roomItems = room.getFloorItems();
        roomItems.addAll(room.getWallItems());
        this.items = new FurniObject[roomItems.size()];
        int index = 0;
        for (HabboItem item : roomItems)
        {
            this.items[index] = new FurniObject(item);
            index++;
        }
    }

    public int insertRoom(Habbo owner)
    {
        this.ownerId = owner.getHabboInfo().getId();
        this.ownerName = owner.getHabboInfo().getUsername();

        try (Connection connection = Emulator.getDatabase().getDataSource().getConnection())
        {
            try (PreparedStatement insertRoomStatement = connection.prepareStatement("INSERT INTO rooms (name, description, model, password, users_max, paper_floor, paper_wall, paper_landscape, thickness_wall, wall_height, thickness_floor, moodlight_data, roller_speed, owner_id, owner_name, override_model) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS))
            {
                insertRoomStatement.setString(1, this.name);
                insertRoomStatement.setString(2, this.description);
                insertRoomStatement.setString(3, this.model);
                insertRoomStatement.setString(4, this.password);
                insertRoomStatement.setInt(5, this.usersMax);
                insertRoomStatement.setString(6, this.paperFloor);
                insertRoomStatement.setString(7, this.paperWall);
                insertRoomStatement.setString(8, this.paperLandscape);
                insertRoomStatement.setInt(9, this.thicknessWall);
                insertRoomStatement.setInt(10, this.wallHeight);
                insertRoomStatement.setInt(11, this.thicknessFloor);
                insertRoomStatement.setString(12, this.moodlightData);
                insertRoomStatement.setInt(13, this.rollerSpeed);
                insertRoomStatement.setInt(14, this.ownerId);
                insertRoomStatement.setString(15, this.ownerName);
                insertRoomStatement.setString(16, this.overrideModel() ? "1" : "0");
                insertRoomStatement.execute();
                try (ResultSet set = insertRoomStatement.getGeneratedKeys())
                {
                    if (set.next())
                    {
                        this.newRoomId = set.getInt(1);
                    }
                }
            }

            if (this.overrideModel())
            {
                try (PreparedStatement insertCustomModelStatement = connection.prepareStatement("INSERT INTO room_models_custom (id, name, door_x, door_y, door_dir, heightmap) VALUES (?, ?, ?, ?, ?, ?)"))
                {
                    insertCustomModelStatement.setInt(1, newRoomId);
                    insertCustomModelStatement.setString(2, "custom_" + newRoomId);
                    insertCustomModelStatement.setInt(3, this.modelObject.doorX);
                    insertCustomModelStatement.setInt(4, this.modelObject.doorY);
                    insertCustomModelStatement.setInt(5, this.modelObject.doorDir);
                    insertCustomModelStatement.setString(6, this.modelObject.heightMap);
                    insertCustomModelStatement.execute();
                }
            }
        }
        catch (Exception e)
        {
            Emulator.getLogging().logErrorLine(e);
        }

        return this.newRoomId;
    }

    public void insertFurniture()
    {
        this.originalToNewFurniIdMap = new THashMap<>();
        this.newToOriginalFurniIdMap = new THashMap<>();

        //Insert All
        try (Connection connection = Emulator.getDatabase().getDataSource().getConnection())
        {
            try (PreparedStatement statement = connection.prepareStatement("INSERT INTO items (user_id, room_id, item_id, wall_pos, x, y, z, rot, extra_data, wired_data, limited_data) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS))
            {
                List<Integer> itemIds = new ArrayList<>();
                for (FurniObject object : this.items)
                {
                    Item baseItem = Emulator.getGameEnvironment().getItemManager().getItem(object.name);

                    if (baseItem == null)
                    {
                        if (!this.missingFurnitureMap.containsKey(object.name))
                        {
                            this.missingFurnitureMap.put(object.name, 0);
                        }

                        this.missingFurnitureMap.put(object.name, missingFurnitureMap.get(object.name) + 1);
                        continue;
                    }

                    statement.setInt(1, this.ownerId);
                    statement.setInt(2, this.newRoomId);
                    statement.setInt(3, baseItem.getId());
                    statement.setString(4, object.wallPosition);
                    statement.setInt(5, object.x);
                    statement.setInt(6, object.y);
                    statement.setDouble(7, object.z);
                    statement.setInt(8, object.rotation);
                    statement.setString(9, object.extradata);
                    statement.setString(10, "");
                    statement.setString(11, object.limitedData);
                    statement.addBatch();
                    itemIds.add(object.id);
                }

                statement.executeLargeBatch();
                try (ResultSet set = statement.getGeneratedKeys())
                {
                    int count = 0;
                    while (set.next())
                    {
                        this.originalToNewFurniIdMap.put(itemIds.get(count), set.getInt(1));
                        this.newToOriginalFurniIdMap.put(set.getInt(1), itemIds.get(count));
                        count++;
                    }
                }
            }
        }
        catch (Exception e)
        {
            Emulator.getLogging().logErrorLine(e);
        }

        for (Map.Entry<Integer, Integer> entry : originalToNewFurniIdMap.entrySet())
        {
            //System.out.println(entry.getKey() + " -> " + entry.getValue());
        }

        try (Connection connection = Emulator.getDatabase().getDataSource().getConnection(); PreparedStatement statement = connection.prepareStatement("UPDATE items SET wired_data = ? WHERE id = ? LIMIT 1"))
        {
            for (FurniObject item : this.items)
            {
                if (item.isWired)
                {
                    //System.out.println(item.wiredData);
                    //if (this.isFurniWired(Emulator.getGameEnvironment().getItemManager().getItem(item.name)))
                    {
                        for (Map.Entry<Integer, Integer> entry : originalToNewFurniIdMap.entrySet())
                        {
                            for (int i = 0; i < parts.length; i++)
                            {
                                for (int j = 0; j < parts.length; j++)
                                {
                                    item.wiredData = item.wiredData.replace(parts[i] + "" + entry.getKey() + "" + parts[j], parts[i] + "" + entry.getValue() + "" + parts[j]);
                                }
                            }
                        }
                    }
                    //System.out.println(item.wiredData);
                    statement.setString(1, item.wiredData);
                    statement.setInt(2, originalToNewFurniIdMap.get(item.id));
                    statement.execute();
                }
            }
        }
        catch (SQLException e)
        {
            Emulator.getLogging().logSQLException(e);
        }

        //Update Wired Furniture
    }

    private static String[] parts = {";", ":", "\t", "\r"};
    private boolean isFurniWired(Item item)
    {
        if (item.getInteractionType().getType() == WiredConditionFurniHaveFurni.class ||
                item.getInteractionType().getType() == WiredConditionFurniHaveHabbo.class ||
                item.getInteractionType().getType() == WiredConditionFurniTypeMatch.class ||
                item.getInteractionType().getType() == WiredConditionMatchStatePosition.class ||
                item.getInteractionType().getType() == WiredConditionNotFurniHaveFurni.class ||
                item.getInteractionType().getType() == WiredConditionNotFurniHaveHabbo.class ||
                item.getInteractionType().getType() == WiredConditionNotFurniTypeMatch.class ||
                item.getInteractionType().getType() == WiredConditionNotMatchStatePosition.class ||
                item.getInteractionType().getType() == WiredConditionTriggerOnFurni.class ||
                item.getInteractionType().getType() == WiredConditionNotTriggerOnFurni.class ||
                item.getInteractionType().getType() == WiredEffectBotWalkToFurni.class ||
                item.getInteractionType().getType() == WiredEffectChangeFurniDirection.class ||
                item.getInteractionType().getType() == WiredEffectLowerFurni.class ||
                item.getInteractionType().getType() == WiredEffectMatchFurni.class ||
                item.getInteractionType().getType() == WiredEffectRaiseFurni.class ||
                item.getInteractionType().getType() == WiredEffectToggleFurni.class ||
                item.getInteractionType().getType() == WiredEffectMatchFurniStaff.class ||
                item.getInteractionType().getType() == WiredEffectMoveFurniAway.class ||
                item.getInteractionType().getType() == WiredEffectMoveFurniTo.class ||
                item.getInteractionType().getType() == WiredEffectMoveFurniTowards.class ||
                item.getInteractionType().getType() == WiredEffectMoveRotateFurni.class ||
                item.getInteractionType().getType() == WiredEffectTeleport.class ||
                item.getInteractionType().getType() == WiredEffectBotTeleport.class ||
                item.getInteractionType().getType() == WiredTriggerFurniStateToggled.class ||
                item.getInteractionType().getType() == WiredTriggerHabboWalkOffFurni.class ||
                item.getInteractionType().getType() == WiredTriggerHabboWalkOnFurni.class ||
                item.getInteractionType().getType() == WiredTriggerBotReachedFurni.class)
        {
            return true;
        }

        return false;
    }

    public int getNewRoomId()
    {
        return this.newRoomId;
    }

    private boolean overrideModel()
    {
        return this.modelObject != null && !this.modelObject.heightMap.isEmpty();
    }
}