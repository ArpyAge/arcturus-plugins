package eu.furnibuilder.hcfix;

import com.eu.habbo.Emulator;
import com.eu.habbo.habbohotel.users.Habbo;
import com.eu.habbo.messages.outgoing.users.UserPermissionsComposer;
import com.eu.habbo.plugin.EventHandler;
import com.eu.habbo.plugin.EventListener;
import com.eu.habbo.plugin.HabboPlugin;
import com.eu.habbo.plugin.events.users.UserLoginEvent;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class HCFix  extends HabboPlugin implements EventListener {

    @Override
    public void onEnable()
    {
        Emulator.getPluginManager().registerEvents(this, this);
        Emulator.getLogging().logStart("Starting HC Fix Plugin!");
    }

    @Override
    public void onDisable()
    {
        Emulator.getLogging().logShutdownLine("Stopping HC Fix Plugin!");
    }


    @Override
    public boolean hasPermission(Habbo habbo, String s)
    {
        return false;
    }

    @EventHandler
    public static void onUserLoginEvent(UserLoginEvent event)
    {
        final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        Runnable runnable = new Runnable() {
            public void run() {
                event.habbo.getClient().sendResponse(new UserPermissionsComposer(event.habbo));
                executorService.shutdown();
            }
        };
        executorService.scheduleWithFixedDelay(runnable, 3, 3, TimeUnit.SECONDS);
    }

    public static void main(String[] args)
    {
        System.out.println("Don't run this seperately");
    }
}