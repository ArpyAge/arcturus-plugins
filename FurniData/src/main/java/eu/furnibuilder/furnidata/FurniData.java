package eu.furnibuilder.furnidata;

import com.eu.habbo.Emulator;
import com.eu.habbo.habbohotel.commands.CommandHandler;
import com.eu.habbo.habbohotel.users.Habbo;
import com.eu.habbo.messages.ICallable;
import com.eu.habbo.plugin.EventHandler;
import com.eu.habbo.plugin.EventListener;
import com.eu.habbo.plugin.HabboPlugin;
import com.eu.habbo.plugin.events.emulator.EmulatorLoadedEvent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class FurniData extends HabboPlugin implements EventListener
{
    public static FurniData INSTANCE = null;

    @Override
    public void onEnable()
    {
        INSTANCE = this;
        Emulator.getPluginManager().registerEvents(this, this);

        if (Emulator.isReady)
        {
            this.checkDatabase();
            ICallable callable1 = new FurnitureClickedEvent();
            Emulator.getGameServer().getPacketManager().registerCallable(Integer.valueOf(99), callable1);
            Emulator.getGameServer().getPacketManager().registerCallable(Integer.valueOf(210), callable1);
        }

        Emulator.getLogging().logStart("[FurniData] Started Furnidata Command Plugin!");
    }

    @Override
    public void onDisable()
    {
        Emulator.getGameServer().getPacketManager().unregisterCallables(99);
        Emulator.getGameServer().getPacketManager().unregisterCallables(210);
        Emulator.getLogging().logShutdownLine("[FurniData] Stopped Furnidata Command Plugin!");
    }

    @EventHandler
    public static void onEmulatorLoaded(EmulatorLoadedEvent event)
    {
        INSTANCE.checkDatabase();
        ICallable callable1 = new FurnitureClickedEvent();
        Emulator.getGameServer().getPacketManager().registerCallable(Integer.valueOf(99), callable1);
        Emulator.getGameServer().getPacketManager().registerCallable(Integer.valueOf(210), callable1);
    }

    @Override
    public boolean hasPermission(Habbo habbo, String s)
    {
        return false;
    }

    private void checkDatabase()
    {
        boolean reloadPermissions = false;

        Emulator.getTexts().register("commands.description.cmd_furnidata", ":furnidata");
        Emulator.getTexts().register("furnidata.cmd_furnidata.keys", "furnidata");
        Emulator.getTexts().register("furnidata.cmd_furnidata.on", "Successfully turned furnidata on!");
        Emulator.getTexts().register("furnidata.cmd_furnidata.off", "Successfully turned furnidata off!");
        reloadPermissions = this.registerPermission("cmd_furnidata", "'0', '1'", "0", reloadPermissions);

        if (reloadPermissions)
        {
            Emulator.getGameEnvironment().getPermissionsManager().reload();
        }

        CommandHandler.addCommand(new FurniDataCommand("cmd_furnidata", Emulator.getTexts().getValue("furnidata.cmd_furnidata.keys").split(";")));
    }

    private boolean registerPermission(String name, String options, String defaultValue, boolean defaultReturn)
    {
        try (Connection connection = Emulator.getDatabase().getDataSource().getConnection())
        {
            try (PreparedStatement statement = connection.prepareStatement("ALTER TABLE  `permissions` ADD  `" + name +"` ENUM(  " + options + " ) NOT NULL DEFAULT  '" + defaultValue + "'"))
            {
                statement.execute();
                return true;
            }
        }
        catch (SQLException e)
        {}

        return defaultReturn;
    }

    public static void main(String[] args)
    {
        System.out.println("Don't run this seperately");
    }
}
